/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package piramide;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nicolas
 */
public class Piramide {
    /**
     * @param args the command line arguments
     */
    private static ArrayList<String> piramides;
    
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * Se lee el archivo desde una ruta ingresada al momento de 
         * ejecutar el programa
         */
        File archivo = new File (args[0]);
        FileReader fr;
        try {
            fr = new FileReader (archivo);
            BufferedReader br = new BufferedReader(fr);
            //Se ejecuta hasta que se acaben los numeros del archivo leido
            do {
                int cubos = Integer.parseInt(br.readLine());
                //Se acota la base para achicar un poco la busqueda
                int base = (int)Math.sqrt(cubos);
                piramide(cubos, base);
                /**
                 * Si tengo elementos en el arreglo, significa que tengo 
                 * soluciones, caso contrario no tengo solucion
                 */
                if (piramides.size() > 0) {
                    for (int i = 0; i < piramides.size(); i++) {
                        System.out.print(piramides.get(i) + " ");
                    }
                    System.out.println("");
                }
                else{
                    System.out.println("Imposible");
                }
            } while (br.ready());
            
        } catch (FileNotFoundException ex) {
          Logger.getLogger(Piramide.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
          Logger.getLogger(Piramide.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    /**
     * Busca todas las posibles combinaciones de piramides, comienza desde la
     * mas grande hasta la mas pequña, siempre generando las piramides mas altas
     * @param cubos cantidad de cubos que se utilizaran
     * @param base base de la piramide, por ejempplo 3
     */
    public static void piramide(int cubos, int base){
        int cubosGlobales = cubos;
        int baseGlobal = base;
        piramides = new ArrayList<>();
        while(baseGlobal > 0){
            
            if (puedoGenerarPiramideAlta(cubos, base)) {
                cubos = restarCubos(cubos,base,"A");
                piramides.add(base + "A");
            }
            if (puedoGenerarPiramideBaja(cubos, base)) {
                cubos = restarCubos(cubos,base,"B");
                piramides.add(base + "B");
            }
            //Significa que encontro la solución
            if (cubos == 0) {
                break;
            }
            //Trata de generar piramides con bases mas pequeñas
            if (base > 2) {
                base -= 1;
            }
            /**
             * Si no puedo encontrar la solucion a partir de una base,
             * tratará de hacerlo a partir de una base mas pequeña
             */
            else{
                piramides = new ArrayList<>();
                baseGlobal -= 1;
                base = baseGlobal;
                cubos = cubosGlobales;
            }
        }
    }
    /**
     * Analiza si se puede generar una piramide alta con los cubos y base dados
     * @param cubos cantidad de cubos con los que se trata de generar la 
     * piramide
     * @param base con la que se trata de generar la piramide
     * @return true en caso afirmativo, false en caso contrario
     */
    private static boolean puedoGenerarPiramideAlta(int cubos, int base) {
        int cubosOcupados = 0;
        //Para asegurarme de que genera una piramide alta de dos niveles
        if (base >= 2) {
            for (int i = base; i > 0; i--) {
                cubosOcupados += (i*i);
            }
            if (cubosOcupados <= cubos) {
                return true;
            }
            else{
                return false;
            }
        }
        return false;
    }
    /**
     * Analiza si se puede generar una piramide baja con los cubos y base dados
     * @param cubos cantidad de cubos con los que se trata de generar la 
     * piramide
     * @param base con la que se trata de generar la piramide
     * @return true en caso afirmativo, false en caso contrario
     */
    private static boolean puedoGenerarPiramideBaja(int cubos, int base) {
        int cubosOcupados = 0;
        //Para asegurarme de que genera una piramide baja de dos niveles
        if (base >= 3) {
            for (int i = base; i > 0; i--) {
                cubosOcupados += (i*i);
                i-=1;
            }
            if (cubosOcupados <= cubos) {
                return true;
            }
            else{
                return false;
            }
        }
        return false;
    }
    
    /**
     * Resta los cubos que se utilizaron para generar un piramide
     * @param cubos cantidad de cubos con los que se trata de generar la 
     * piramide
     * @param base con la que se trata de generar la piramide
     * @param categoria "A" si es una piramide alta, si no, es una piramide baja
     * @return la cantidad de cubos restantes
     */
    private static int restarCubos(int cubos, int base, String categoria) {
        int cubosOcupados = 0;
        if (categoria.equals("A")) {
            for (int i = base; i > 0; i--) {
                cubosOcupados += (i*i);
            }
        }
        else{
            for (int i = base; i > 0; i--) {
                cubosOcupados += (i*i);
                i-=1;
            }
        }
        return cubos - cubosOcupados;
    }
}
